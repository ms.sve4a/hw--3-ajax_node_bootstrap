const contact = document.querySelector('.button-contact');

contact.addEventListener('click', function(event){
  event.preventDefault();

  setCookie('experiment', 'novalue', {'max-age': 300});

  if(!getCookie('new-user')) {
    setCookie('new-user', 'true');
  } else {
    setCookie('new-user', 'false');
  }
});



function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}



function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    ...options
  };

  if (options.expires && options.expires.toUTCString) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}
